<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Introduction</title>
		<link rel="stylesheet" href="../css/style.css">
	</head>
	<body>

		<div class="site-header">
			<a href="../">&laquo; Gulp by Examples</a>
		</div>

		<div class="main-content">
<h1 id="introduction">Introduction</h1>
<h2 id="overview">Overview</h2>
<p>We&#39;ll look into what <a href="https://gulpjs.com/">Gulp.js</a> is and what it can do for us and how.</p>
<h2 id="why-build-systems">Why build systems?</h2>
<p>When you write JS, CSS etc. you often need to minify them. You can do them by hand, right? But when you need to edit them multiple times a day and need to deploy fast to client server, it gets on you. This puts burden on an already difficult task of developing and testing the app. Do this multiple times a day and it adds up to the time, cost and effort.</p>
<p>Plus, there could be mistakes. Humans are bound to do mistakes. So this gets even messier.</p>
<p>Build systems are created to automate those tasks for you. Computers are good at repeatitive tasks which should result in exactly the expected output everytime, without mistakes. So build systems will save you time and remove a lot of annoyances once you set it up.</p>
<p>Build systems made for web devs are usually powered by <a href="https://nodejs.org/">Node.js</a>. Gulp is made with Node too. So you can run Gulp on Windows, Mac, Linux, Raspberry Pi (ARM) etc. and even on (some) servers.</p>
<h2 id="why-gulp">Why Gulp?</h2>
<p>There are other build systems out there. Popular ones are <a href="https://gruntjs.com/">Grunt</a>, <a href="https://webpack.js.org/">Webpack</a> (a.k.a. static asset bundler) and some even use <a href="https://deliciousbrains.com/npm-build-script/">npm scripts</a> (by using <code>scripts</code> key within <code>package.json</code> to define individual cli tasks).</p>
<p>Gulp has a more of what I would say a &quot;factory belt&quot; perspective of how a build system should work. It takes some file, pipes the content to the next process in line, when it&#39;s finished it pipes the content to the next process and so on... just like a production system on a factory. We see things being passed from one process to the other. And when it finishes, we get a final product.</p>
<pre><code>...
  gulp.src(&#39;./src/assets/css/*.css&#39;)
  .pipe(minifyCSS())
  .pipe(gulp.dest(&#39;./dist/assets/css&#39;));</code></pre><p>It takes so little time to guess what this code does. It takes css files, minifies them, then puts the result into <code>./dist/assets/css</code>.</p>
<p>The same thing on Grunt would be something like this:</p>
<pre><code>cssmin: {
  target: {
    files: [{
      expand: true,
      cwd: &#39;src/assets/css&#39;,
      src: [&#39;*.css&#39;, &#39;!*.min.css&#39;],
      dest: &#39;dist/assets/css&#39;,
      ext: &#39;.min.css&#39;
    }]
  }
}</code></pre><p>So boring, dry and hard to swallow!</p>
<p>Gulp scripts on the other hand are very easy to understand. You see the code, you understand what it does right away. You make edits and go on with your life. No hassle, no fuss! No spending time learning loads of docs for just using a build tool.</p>
<h3 id="why-not-gulp">Why &quot;not&quot; Gulp?</h3>
<p>Someone might be inclined not to use Gulp because there are some limitations, as there are in anything within the universe. Gulp is not flawless and might not be suitable for your workflow:</p>
<ul>
<li>If your co-workers don&#39;t want to use it.</li>
<li>If you need some latest functionality that gulp based plugins did not implement yet (the author may have lost interest or made it deprecated).</li>
<li>If you like other build systems better. We all have taste and needs different than others. So, it&#39;s ok.</li>
<li>If your specific project does not need Gulp or there is a better build system for that particular one.</li>
</ul>
<p>Gulp is mainly made for web projects, so you may have better time with other build systems for other kind of projects. Such as, <code>make</code> is better for C/C++ code.</p>
<h2 id="installation">Installation</h2>
<p>Follow these 2:</p>
<h4 id="1-systemwide">1) Systemwide:</h4>
<ul>
<li>Install <a href="https://nodejs.org/">Node.js</a> if you haven&#39;t done it yet.</li>
<li>Check to see if <code>node --version</code> returns anything</li>
<li>Make sure you have <a href="https://www.npmjs.com/">npm</a> by running <code>npm --version</code></li>
<li>Then install Gulp CLI with <code>npm install -g gulp-cli</code></li>
</ul>
<h4 id="2-for-project">2) For project:</h4>
<ul>
<li><code>cd</code> to a new project directory</li>
<li>then run <code>npm init</code></li>
<li><code>npm install gulp -D</code></li>
<li>create a new <code>gulpfile.js</code> and enter your script there</li>
</ul>
<h2 id="using-different-package-managers">Using different package managers</h2>
<p>There are many package managers for Node.js. <strong>npm</strong> is usually bundled with Node.js and it&#39;s considered to be the most mature. There is also <strong>Yarn</strong> which is made by Facebook to tackle <a href="https://www.freecodecamp.org/news/javascript-package-managers-101-9afd926add0a/">some of the downsides</a> that npm has. I recently discovered <a href="https://github.com/pnpm/pnpm">pnpm</a> which works the same way as npm does, but saves disk space by keeping a package directory only once on the disk and symlinking on others. npm copies each dependency dir on each project you work on and sometimes there are nesting dependencies (<code>node_modules/some_module/node_modules/some_bunch_of_other_nesting_deps</code>) which is nasty to look at. Yarn solved this by not nesting dep directories. But pnpm is way ahead when saving disk space. It just symlinks the dep directories from one central place and you have a wonderful life.</p>
<ul>
<li>For normies just starting out, go with npm. In some operating systems it&#39;s installed by default with Node.</li>
<li>For people who dislike npm, use Yarn.</li>
<li>For those who think storing one dep directory multiple places without reason is dumb and wants to save space, use pnpm. (It&#39;s my personal favorite too!)</li>
</ul>
<p>There are countless others. Feel free to install and experiment. Don&#39;t forget to read their docs on how to use them.</p>
<p>This site shows only npm commands, but they can be used with any other package manager of your choice. Afterall, they are all installing the same packages. But please remember, mixing package managers is not recommended. Use the same one for the project that you started it with.</p>
<h3 id="yarn">Yarn</h3>
<p>If you want to use it you&#39;ll have to notice that Yarn has different meaning for <a href="https://classic.yarnpkg.com/en/docs/cli/install"><code>yarn install</code></a> and <a href="https://classic.yarnpkg.com/en/docs/cli/add/"><code>yarn add</code></a>. So <code>npm install</code> would be <code>yarn install</code>. But <code>npm install -D gulp</code> would be:</p>
<pre><code>yarn add -D gulp</code></pre><p><code>yarn install</code> installs all deps from package.json. <br><code>yarn add ...</code> installs a dep.</p>
<p>It makes sense, since we <code>remove</code> a dep, not <em>uninstall</em>. Opposite of <code>remove</code> should be <code>add</code>, not <em>install</em>.</p>
<p>Yarn uses a lock file called <code>yarn.lock</code> that has all the version numbers for the packages, so that it installs exactly those ones on any other computer in the world making it replicate the exact environment on all of the copies. So it makes it easier to collaborate. Nobody will have a package one version higher than you and face a bug or output results differently than yours. <strong>Note:</strong> npm does not do this by default and has a <a href="https://docs.npmjs.com/cli/shrinkwrap"><code>npm shrinkwrap</code></a> command to do this manually yourself.</p>
<h3 id="pnpm">pnpm</h3>
<p>If you want to use pnpm, you&#39;re in luck. pnpm commands are same as npm. Just add a &quot;p&quot; in front of <code>npm</code> commands and you should be good to go! So <code>npm install -D gulp</code> becomes:</p>
<pre><code>pnpm install -D gulp</code></pre><p>It also produces a <code>pnpm-lock.yaml</code> so that it&#39;s easier to replicate the same packages with same versions on any other computer in the world. <strong>Note:</strong> npm does not do this by default and has a <a href="https://docs.npmjs.com/cli/shrinkwrap"><code>npm shrinkwrap</code></a> command to do this manually yourself.</p>
<h2 id="for-prehistoric-dinosaurs-who-liked-classic-javascript-better-and-doesnt-think-node-is-a-good-idea">For &quot;prehistoric dinosaurs&quot; who liked classic Javascript better and doesn&#39;t think Node is a good idea</h2>
<p>Read this: <a href="https://medium.com/the-node-js-collection/modern-javascript-explained-for-dinosaurs-f695e9747b70">Modern JavaScript Explained For Dinosaurs</a></p>
<p>To be honest, I didn&#39;t like Node based tools either. I thought it was bloat, installing countless packages on my computer for nothing. It also seemed like unnecessary complication of the simple html/css/js stuff. But Node saves a lot of time on modern web dev projects and enables cli tools to use js like never before.</p>
<p>Modern web developers have to do more stuff than they used to in the old times. Minifying, gzipping, transpilation are fairly new concepts that were introduced in comparatively recent times. A modern web developer has to do more than the devs of the 90s and 00s. What tool would you use for doing these? Node understands web dev languages better than any other tool. It is made by web for the web. So use it. If you like it, use it some more. If you don&#39;t use it, you&#39;re surely missing out on the fun.</p>

		</div><!-- end of .main-content -->
		<div class="bottom-links">
			<div class="link left">
			
			&nbsp;</div>
			<div class="link center">
				<a href="../">Back to Home</a>
			</div>
			<div class="link right">&nbsp;
			
				<a href="../new-project">&raquo; Next: Starting a new project</a>
			
			</div>
		</div>
		<div class="site-footer">
			Gulp by Examples | Source code is available at <a href="https://gitlab.com/adnan360/gulp-examples">GitLab</a>
		</div>
	</body>
</html>