# Gulp by Examples

![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

Examples to be used with Gulp.js build system. Markdown version of the Examples are there inside the `articles` directory. This repo is designed in such a way that you can either (1) visit the [public site here](https://adnan360.gitlab.io/gulp-examples) or (2) clone the repo and read the README.md files inside the article folders.

To view the site locally, just:

```
npm install -g simple-server # <- run once
npx simple-server public 3300
```

For instructions on developing this project see `DEVELOPERS.md`.


## License
Code specific to this repository is licensed under CC0. Do whatever you want with it.