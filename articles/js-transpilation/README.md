# JS Transpilation

## Overview

We'll have a look at how we can use latest ES code before browsers add support for it.


## What's the hype about ES6, ES7, ES2015?

ES stands for ECMAScript. They are different versions of the standards that JavaScript follows. For details you can read [this article](https://codeburst.io/javascript-wtf-is-es6-es8-es-2017-ecmascript-dca859e4821c). Each ECMAScript version sets the standard on how code will be written to be compatible with that version. Most modern browsers support ES6. But some customers might have older browsers, so targetting ES5 would be a better option if you're concerned.

What about more latest versions then? Well, browsers are slow to implement the latest standards. Check [this page](https://www.w3schools.com/Js/js_versions.asp) to see the status of the support on browsers. So if you want to use latest coding style on your JS files, you'll have to use a thing called a transpiler. It will convert your latest code into an older standard so that more browsers can be supported.

The most commonly used JavaScript transpiler is [Babel.js](https://babeljs.io/).


## Basic usage

We'll use [gulp-babel](https://www.npmjs.com/package/gulp-babel) here.

1. `npm install -D gulp-babel @babel/core @babel/preset-env`
2. `require` it on your `gulpfile.js`: `const babel = require('gulp-babel');`
3. Add task:
```
gulp.task('js', async function () {
	gulp.src('./src/js/*.js')
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(gulp.dest('./dist/js'));
});
```

Put some latest JS code into `src/js/script.js`, like this:

```
var name = "Bob", time = "today";
console.log(`Hello ${name}, how are you ${time}?`);
```

If you run `gulp js` it should transpile your code into `dist/js`. If you load it on a browser it should say `Hello Bob, how are you today?` on web dev Console.


### Further Reading
- [JavaScript — WTF is ES6, ES8, ES 2017, ECMAScript… ?](https://codeburst.io/javascript-wtf-is-es6-es8-es-2017-ecmascript-dca859e4821c)
- [JavaScript Versions](https://www.w3schools.com/Js/js_versions.asp)

