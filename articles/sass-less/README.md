# Using SASS and LESS for pure CSS comfort

## Overview

We get to know how to use Sass and Less CSS preprocessors to make our life easier.


## What are these preprocessors?

Sass and Less adds variables and nesting styles into CSS which is missing on the browser's end. It makes life so much easier, for example, you can declare a color in a variable throughout your CSS and when you need to apply a slightly darker color, don't need to pick a color yourself! These pre-processors can make the color darker for you! Oh, the comfort!

If you handle CSS often and want to become a CSS jedi, this is the tool for you.


## Sass

We'll use [`gulp-sass`](https://www.npmjs.com/package/gulp-sass).

1. `npm install node-sass gulp-sass -D`
2. `require` these \
```
...
var sass = require('gulp-sass');
sass.compiler = require('node-sass');
```
3. Add task: \
```
...
gulp.task('sass', async function () {
  gulp.src('./src/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});
...
```

Now add some `style.scss` file inside `./src/sass`:
```
$mycolor: green;
body {
  background: $mycolor;
}
```

Don't forget to add it within your `<head>` tag on html:
```
<link rel="stylesheet" href="css/style.css">
```

When you run `gulp sass` it should put this into `dist/css`:
```
body {
  background: green;
}
```

Optionally, you can add:
```
gulp.task('watch', async function () {
	...
	gulp.watch('./sass/*.scss', ['sass']);
});
```

This should automatically update to `dist` and you'll be able to see the results just hitting F5 without reaching the terminal.

Wait! You can be lazier! You can setup a server, like `gulp-connect` with livereload, so that it reloads automatically when there is a change! You'll be able to save thousands of seconds of pressing F5 with this.


## Less

1. Install [gulp-less](https://www.npmjs.com/package/gulp-less).
```
npm install gulp-less
```
2. `require` stuff:
```
...
var less = require('gulp-less');
var path = require('path');
```
3. Add task:
```
...
gulp.task('less', async function () {
	gulp.src('./src/less/**/*.less')
		.pipe(less({
			paths: [ path.join(__dirname, 'src', 'less', 'includes') ]
		}))
		.pipe(gulp.dest('./dist/css'));
});
```

The `paths` is to set the path for the files you want to `@import`.

You can also use [other plugins](https://www.npmjs.com/package/gulp-less#using-plugins), such as autoprefix.


### Further Reading:
- [`gulp-sass` plugin page](https://www.npmjs.com/package/gulp-sass)
- [`gulp-less` plugin page](https://www.npmjs.com/package/gulp-less)
