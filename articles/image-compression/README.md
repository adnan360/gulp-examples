# Image Compression

## Overview

We'll see how to apply compression to images automatically so that they load faster.


## Basic usage

We can use [`gulp-imagemin`](https://www.npmjs.com/package/gulp-imagemin):

1. `npm install -D gulp-imagemin`
2. `require` it on `gulpfile.js`: `const imagemin = require('gulp-imagemin');`
3. Use it:
```
gulp.task('images', async function () {
	gulp.src('src/images/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/images'))
});
```

Now if you run `gulp images` it will take the images in `src/images`, compress JPG, PNG, GIF and SVGs, then output to `dist/images`.


## Custom options

```
...
.pipe(imagemin([
	imagemin.gifsicle({interlaced: true}),
	imagemin.mozjpeg({quality: 75, progressive: true}),
	imagemin.optipng({optimizationLevel: 5}),
	imagemin.svgo({
		plugins: [
			{removeViewBox: true},
			{cleanupIDs: false}
		]
	})
]))
...
```

For more options that can be used, check out the links below.


### Further reading
- [gulp-imagemin plugin page](https://www.npmjs.com/package/gulp-imagemin)
- [imagemin-gifsicle](https://github.com/imagemin/imagemin-gifsicle)
- [imagemin-mozjpeg](https://github.com/imagemin/imagemin-mozjpeg)
- [imagemin-optipng](https://github.com/imagemin/imagemin-optipng)
- [imagemin-svgo](https://github.com/imagemin/imagemin-svgo)
