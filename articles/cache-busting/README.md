# Cache Busting

## Overview

We'll learn how to bypass browser cache and show the latest version of our css/js for every user.


## What is Cache Busting?

Well, we usually name our styles `style.css` and our js as `script.js`. When a user loads the page it downloads them and keeps them in cache in case the page is accessed again and then it can show the site faster. This is fine as long as the files don't change so often.

When you change these files often, however, the browser still uses the old file from the cache. And this is where the problem starts. Sometimes the browser gets so stubborn that even when users refresh the page, it does not fetch the newer file. The only way is to do a hard refresh (Shift+F5 / Ctrl+Shift+R) or use a private/incognito window. It's not always possible to tell every user to do a hard refresh on their browsers or use incognito everytime you update your css/js. That would be insane and impractical!

So the better solution is to use cache busting.

There are many techniques, but the most recognized is to add a unique hash for each version (which changes with each edits to the file). e.g. `<link rel="stylesheet" href="css/style.css?hash=123something456">`. When the file changes, we append a different hash, like `<link rel="stylesheet" href="css/style.css?hash=123somethingelse789">`. This way it goes to the same file, but the hash tricks the browser into thinking that it's a different file. So it does not use the cache and downloads the file anew.

There are skeptics of this method and some suggest to use different filenames instead. But using hash is easier, so we'll go with this for now.


## Basic usage

We can use [gulp-cache-bust](https://www.npmjs.com/package/gulp-cache-bust). It's easy to use and needs no config.

1. `npm install -D gulp-cache-bust`
2. `require` it on your `gulpfile.js`: `const cacheBuster = require('gulp-cache-bust');`
3. Use it:
```
...
gulp.task('html', async function () {
	gulp.src('./src/*.html')
		.pipe(cacheBuster())
		.pipe(gulp.dest('./dist'));
});
```

This will go through your html files and add the MD5 hashes based on the file content that is linked.

**Note:** You'll have to run this after you've generated your css/js files that you want to be cache-less. Otherwise, the hash will be of the old files. e.g. your `gulpfile.js` default task may be arranged like this:

```
gulp.task('default', async function(){
	gulp.series('css','js','html')();
});
```

