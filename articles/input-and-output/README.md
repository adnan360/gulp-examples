# Input and Output

## Overview

We'll get an idea on how inputs and outputs work in Gulp build system.


## Input and output

Every build system has something that we provide it (input) and something that it spits out (output).

Gulp is similar. [Typically](https://stackoverflow.com/questions/23730882/what-is-the-role-of-src-and-dist-folders) you have your source files in a directory (usually `src`) and you will put your outputs in another directory (usually `dist`). In the software industry it is important to not use the same directory as input and output. This is mainly to keep the input clean and keeping it intact no matter what we do inside the output folder. We might mess up, you know!

Let's create 2 folders inside our project directory: `src` and `dist`. So your directory might look something like this:

```
 .
 ├─gulpfile.js
 ├─node_modules
 ├─package.json
 ├─dist
 └─src
```

Now create an `index.html` file inside `src` directory:

```
...
 └─src
    └─index.html
```

Put some html inside it, like:

```
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Some test</title>
	</head>
	<body>
		<p>This is an HTML file.</p>
	</body>
</html>
```

Now go into your `gulpfile.js` and add this task:

```
gulp.task('html', async function () {
	gulp.src('./src/*.html') // take original files
		.pipe(gulp.dest('./dist')); // put them on destination folder
});
```

Before running anything check your `dist` folder. It should be empty, since you just created it. Now run `gulp html`. You will see that the `dist` directory has the [same file](dist/index.html) you created on `src` folder. Awesome! You've just written your first functional build file!

**Explanation:**
The task uses `gulp.src()` to take something as input, in our case all the `.html` files inside `src`. The file data is then `pipe`d to `gulp.dest()` to output to our destination directory, in our case `dist` directory.

Piping is very common in gulp scripts. We use `pipe` to move content from one command to the next. For example, we may `src` the css files, then pipe it's content to `cleanCSS` function to be minified, then pipe the results into `gulp.dest` to be written to destination directory.

```
gulp.src()  ->  cleanCSS() minification  ->  gulp.dest()
```

But we'll visit that later on [another article](../modules/).

A little housekeeping. Add this `html` task in the `gulp.series()` function that we added earlier so that it runs by default from now on:

```
gulp.task('default', async function(){
	...
	gulp.series('css','js','html')();
	...
```

Now if you run `gulp` it should run all 3 of the tasks.
