const gulp = require('gulp');

gulp.task('css', async function () {
	console.log('This will be some css tasks running in the future');
});

gulp.task('js', async function () {
	console.log('This will be some js tasks running in the future');
});

gulp.task('html', async function () {
	gulp.src('./src/*.html') // take original files
		.pipe(gulp.dest('./dist')); // put them on destination folder
});

gulp.task('default', async function(){
	console.log('This is the default task running!! Hehe!!');
	gulp.series('css','js','html')();
});