# Introduction

## Overview

We'll look into what [Gulp.js](https://gulpjs.com/) is and what it can do for us and how.


## Why build systems?

When you write JS, CSS etc. you often need to minify them. You can do them by hand, right? But when you need to edit them multiple times a day and need to deploy fast to client server, it gets on you. This puts burden on an already difficult task of developing and testing the app. Do this multiple times a day and it adds up to the time, cost and effort.

Plus, there could be mistakes. Humans are bound to do mistakes. So this gets even messier.

Build systems are created to automate those tasks for you. Computers are good at repeatitive tasks which should result in exactly the expected output everytime, without mistakes. So build systems will save you time and remove a lot of annoyances once you set it up.

Build systems made for web devs are usually powered by [Node.js](https://nodejs.org/). Gulp is made with Node too. So you can run Gulp on Windows, Mac, Linux, Raspberry Pi (ARM) etc. and even on (some) servers.


## Why Gulp?

There are other build systems out there. Popular ones are [Grunt](https://gruntjs.com/), [Webpack](https://webpack.js.org/) (a.k.a. static asset bundler) and some even use [npm scripts](https://deliciousbrains.com/npm-build-script/) (by using `scripts` key within `package.json` to define individual cli tasks).

Gulp has a more of what I would say a "factory belt" perspective of how a build system should work. It takes some file, pipes the content to the next process in line, when it's finished it pipes the content to the next process and so on... just like a production system on a factory. We see things being passed from one process to the other. And when it finishes, we get a final product.

```
...
  gulp.src('./src/assets/css/*.css')
  .pipe(minifyCSS())
  .pipe(gulp.dest('./dist/assets/css'));
```

It takes so little time to guess what this code does. It takes css files, minifies them, then puts the result into `./dist/assets/css`.

The same thing on Grunt would be something like this:

```
cssmin: {
  target: {
    files: [{
      expand: true,
      cwd: 'src/assets/css',
      src: ['*.css', '!*.min.css'],
      dest: 'dist/assets/css',
      ext: '.min.css'
    }]
  }
}
```

So boring, dry and hard to swallow!

Gulp scripts on the other hand are very easy to understand. You see the code, you understand what it does right away. You make edits and go on with your life. No hassle, no fuss! No spending time learning loads of docs for just using a build tool.


### Why "not" Gulp?

Someone might be inclined not to use Gulp because there are some limitations, as there are in anything within the universe. Gulp is not flawless and might not be suitable for your workflow:

- If your co-workers don't want to use it.
- If you need some latest functionality that gulp based plugins did not implement yet (the author may have lost interest or made it deprecated).
- If you like other build systems better. We all have taste and needs different than others. So, it's ok.
- If your specific project does not need Gulp or there is a better build system for that particular one.

Gulp is mainly made for web projects, so you may have better time with other build systems for other kind of projects. Such as, `make` is better for C/C++ code.


## Installation

Follow these 2:

#### 1) Systemwide:
- Install [Node.js](https://nodejs.org/) if you haven't done it yet.
- Check to see if `node --version` returns anything
- Make sure you have [npm](https://www.npmjs.com/) by running `npm --version`
- Then install Gulp CLI with `npm install -g gulp-cli`

#### 2) For project:
- `cd` to a new project directory
- then run `npm init`
- `npm install gulp -D`
- create a new `gulpfile.js` and enter your script there


## Using different package managers

There are many package managers for Node.js. **npm** is usually bundled with Node.js and it's considered to be the most mature. There is also **Yarn** which is made by Facebook to tackle [some of the downsides](https://www.freecodecamp.org/news/javascript-package-managers-101-9afd926add0a/) that npm has. I recently discovered [pnpm](https://github.com/pnpm/pnpm) which works the same way as npm does, but saves disk space by keeping a package directory only once on the disk and symlinking on others. npm copies each dependency dir on each project you work on and sometimes there are nesting dependencies (`node_modules/some_module/node_modules/some_bunch_of_other_nesting_deps`) which is nasty to look at. Yarn solved this by not nesting dep directories. But pnpm is way ahead when saving disk space. It just symlinks the dep directories from one central place and you have a wonderful life.

- For normies just starting out, go with npm. In some operating systems it's installed by default with Node.
- For people who dislike npm, use Yarn.
- For those who think storing one dep directory multiple places without reason is dumb and wants to save space, use pnpm. (It's my personal favorite too!)

There are countless others. Feel free to install and experiment. Don't forget to read their docs on how to use them.

This site shows only npm commands, but they can be used with any other package manager of your choice. Afterall, they are all installing the same packages. But please remember, mixing package managers is not recommended. Use the same one for the project that you started it with.


### Yarn

If you want to use it you'll have to notice that Yarn has different meaning for [`yarn install`](https://classic.yarnpkg.com/en/docs/cli/install) and [`yarn add`](https://classic.yarnpkg.com/en/docs/cli/add/). So `npm install` would be `yarn install`. But `npm install -D gulp` would be:
```
yarn add -D gulp
```

`yarn install` installs all deps from package.json. \
`yarn add ...` installs a dep.

It makes sense, since we `remove` a dep, not _uninstall_. Opposite of `remove` should be `add`, not _install_.

Yarn uses a lock file called `yarn.lock` that has all the version numbers for the packages, so that it installs exactly those ones on any other computer in the world making it replicate the exact environment on all of the copies. So it makes it easier to collaborate. Nobody will have a package one version higher than you and face a bug or output results differently than yours. **Note:** npm does not do this by default and has a [`npm shrinkwrap`](https://docs.npmjs.com/cli/shrinkwrap) command to do this manually yourself.


### pnpm

If you want to use pnpm, you're in luck. pnpm commands are same as npm. Just add a "p" in front of `npm` commands and you should be good to go! So `npm install -D gulp` becomes:
```
pnpm install -D gulp
```

It also produces a `pnpm-lock.yaml` so that it's easier to replicate the same packages with same versions on any other computer in the world. **Note:** npm does not do this by default and has a [`npm shrinkwrap`](https://docs.npmjs.com/cli/shrinkwrap) command to do this manually yourself.


## For "prehistoric dinosaurs" who liked classic Javascript better and doesn't think Node is a good idea

Read this: [Modern JavaScript Explained For Dinosaurs](https://medium.com/the-node-js-collection/modern-javascript-explained-for-dinosaurs-f695e9747b70)

To be honest, I didn't like Node based tools either. I thought it was bloat, installing countless packages on my computer for nothing. It also seemed like unnecessary complication of the simple html/css/js stuff. But Node saves a lot of time on modern web dev projects and enables cli tools to use js like never before.

Modern web developers have to do more stuff than they used to in the old times. Minifying, gzipping, transpilation are fairly new concepts that were introduced in comparatively recent times. A modern web developer has to do more than the devs of the 90s and 00s. What tool would you use for doing these? Node understands web dev languages better than any other tool. It is made by web for the web. So use it. If you like it, use it some more. If you don't use it, you're surely missing out on the fun.
