# Code Injection

## Overview

We'll take a look at how we can insert code snippets into our output files.

This is especially useful if you use analytics tools (like Google Analytics, Matomo, Open Web Analytics etc.) and have to inject some code into your html files. Adding before `</head>` or `</body>` is supported. So this makes your workflow extremely easy. Plus it gives you flexibility if you ever want to change your analytics tool.

There are other uses for this. If you think you need this, then I guess you already know why you need it. :)


## Basic usage

There are many plugins out there. We'll use [gulp-inject-string](https://www.npmjs.com/package/gulp-inject-string) here.

1. `npm install -D gulp-inject-string`
2. `require` it on your `gulpfile.js`: `const inject = require('gulp-inject-string');`
3. Add task:
```
gulp.task('inject', async function(){
    gulp.src('src/index.html')
        .pipe(inject.before('</body', '<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>\n'))
        .pipe(inject.after('</title>', '\n<link rel="stylesheet" href="test.css">\n'))
        .pipe(gulp.dest('dist'));
});
```

Now you can run `gulp inject` to run the task. This will either add a code before or after a certain line.


### Further Reading
- [gulp-inject-string package](https://www.npmjs.com/package/gulp-inject-string)
