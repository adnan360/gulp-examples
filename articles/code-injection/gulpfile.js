const gulp = require('gulp');
const inject = require('gulp-inject-string');

gulp.task('inject', async function(){
	gulp.src('src/index.html')
		.pipe(inject.before('</body', '<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>\n'))
		.pipe(inject.after('</title>', '\n<link rel="stylesheet" href="test.css">\n'))
		.pipe(gulp.dest('dist'));
});

gulp.task('default', async function(){
	gulp.src('src/*.css')
		.pipe(gulp.dest('dist'));
	gulp.series('inject')();
});