# Setting up a server

## Overview

We'll see how we can run a server to serve the generated files and automatically update when files change, saving us millions of key presses.


## Basic usage

We can use [`gulp-connect`](https://www.npmjs.com/package/gulp-connect). The plugin is sponsored by JetBrains, so you're in good hands.

1. `npm install -D gulp-connect`
2. `require` it within `gulpfile.js`: `const connect = require('gulp-connect');`
3. Add task:
```
...
gulp.task('server', async function() {
	connect.server();
});
```

Run `gulp server` to see how it works.

You can also use LiveReload like this:
```
gulp.task('sometask', async function () {
	gulp.src(...)
		...
		.pipe(gulp.dest(...))
		.pipe(connect.reload());
});
...
gulp.task('server', async function() {
	connect.server({
		livereload: true // auto refresh without hitting f5
	});
});
```

`.pipe(connect.reload()` is necessary for the livereload. You'll have to add it where you want it to reload automatically, especially where some files change. But you'll have to run `gulp sometask` to trigger livereload. We'll fix that in a moment.

You can also add other options to `connect()`:

```
...
	connect.server({
		root: 'dist', // where your destination files are
		livereload: true, // auto refresh without hitting f5
		port: 4400 // the port to be used
	});
...
```

Honestly speaking, this is my go to snippet right here:

```
gulp.task('css', async function () {
	gulp.src(...)
		...
		.pipe(gulp.dest(...))
		.pipe(connect.reload());
});
...
gulp.task('watch', async function () {
	gulp.watch('./css/*.css', gulp.series('css'));
	//...
});

gulp.task('server', async function() {
	// Start the server
	connect.server({
		root: 'dist',
		livereload: true,
		port: 4400
	});
	// Watch for changes
	gulp.series('watch')();
});
```

Adding `watch` eliminates the need to go to cli everytime I make a change. I make the change, it triggers `watch` and it livereloads.

Another thing we can improve is that it does not open the localhost url on browser automatically. We have to manually copy-paste the url into browser. We can add [gulp-open](https://www.npmjs.com/package/gulp-open) to improve this:

1. `npm install gulp-open -D`
2. Add `const open = require('gulp-open');`
3. Add the `open` command after `connect`:
```
gulp.task('server', async function() {
	// Start the server
	connect.server({
		root: 'dist',
		livereload: true,
		port: 4400
	});
	// Watch for changes
	gulp.series('watch')();
	// Open on web browser
	gulp.src(__filename)
	.pipe(open({uri: 'http://localhost:4400'}));
});
```

Now if you run `gulp server` it should open the site on your browser.

Everything happens without a keystroke. Ummm, comfort!


### Further Reading:
- [`gulp-connect` plugin page](https://www.npmjs.com/package/gulp-connect)
- [`gulp-open` plugin page](https://www.npmjs.com/package/gulp-open)
