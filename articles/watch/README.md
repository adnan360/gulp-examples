# Watch files for changes

## Overview

We'll see how to do something when certain files change.


## Basic usage

Watch feature is built into Gulp, so need to seek for a plugin. Just do something like:
```
...
gulp.task('watch', async function () {
	gulp.watch('./src/*.css', gulp.series('css'));
});
```

This will watch the `.css` files inside `src` if they are edited. Whenever it detects a change, it will run the task `css`. You can use this feature to run minify, linting, compression or even livereload. So your life becomes much easier. You will have to run `gulp ...` commands almost never, and everything magically updates when you make changes to files.

You can extend this to anything you like:

```
gulp.task('css', async function () {
	// run minify, sass pre process, server livereload etc.
});

gulp.task('js', async function () {
	// run linting, babel (es2015) transliteration, minify/compression etc.
});

gulp.task('html', async function () {
	// run minify/compression, cache busting etc.
});

...
gulp.task('watch', async function () {
	gulp.watch('./src/*.css', gulp.series('css'));
	gulp.watch('./src/*.js', gulp.series('js'));
	gulp.watch('./src/*.html', gulp.series('html'));
});
```


### Further Reading
- [Gulp docs on `watch()`](https://gulpjs.com/docs/en/api/watch)
