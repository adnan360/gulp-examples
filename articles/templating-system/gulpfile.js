const gulp = require('gulp');
const nunjucks = require('gulp-nunjucks');

gulp.task('template', async function() {
	gulp.src('src/*.html')
		.pipe(nunjucks.compile({name: 'John Doe'}))
		.pipe(gulp.dest('dist'))
});

gulp.task('default', async function(){
	gulp.series('template')();
});