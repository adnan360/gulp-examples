# Templating System

## Overview

We'll see how we can use Nunjucks templates to improve the flexibility of our outputs.

There are many templating systems out there for gulp. But here we're focusing on Nunjucks.

Nunjucks is a feature-rich templating system. See their [website](https://mozilla.github.io/nunjucks/) and [docs](https://mozilla.github.io/nunjucks/templating.html) for examples and features.

Templating let's us use separate files for separate design elements in our HTML pages. For example, you can have separate HTML files for header and footer. Plus we can pass variable values to the templates without modifying the templates themselves. This gives us more flexibility in our output.


## Basic usage

Let's see a basic usage example:

1. `npm install -D gulp-nunjucks`
2. `require` it on your `gulpfile.js`: `const nunjucks = require('gulp-nunjucks');`
3. Add task:
```
gulp.task('template', async function() {
	gulp.src('src/templates/greeting.html')
		.pipe(nunjucks.compile({name: 'John Doe'}))
		.pipe(gulp.dest('dist'))
});
```
4. Create a `src/templates/greeting.html` file and put [this](src/templates/greeting.html).

Now you can run `gulp template` to run the task. This will compile the templates according to your settings and produce the output on `dist` directory. This also passes a variable `name` to template. But if you don't need to pass any variables, you can get rid of `{name: 'John Doe'}` for your use case.

[This directory](https://gitlab.com/adnan360/gulp-examples/-/tree/master/articles/templating-system) in the source tree also has a basic layout example for a website. You can see an example output produced <a href="dist/index.html" target="_blank">here</a>.


### Further Reading
- [gulp-nunjucks package](https://www.npmjs.com/package/gulp-nunjucks)
