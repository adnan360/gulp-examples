# Starting a new project

## Overview

We'll get our feet wet into Gulp and learn how things work.


## Starting a new project

- Create a new directory
- Open the terminal on the directory
- Assuming you have NPM and Gulp CLI installed (if not, check [Introduction article](../intro)), you should now run `npm init` so that npm can manage what you install on this directory. It will ask some questions. The questions asked can be answered, or not. They are optional. Just keep pressing enter and you should be good for now.
- Now install `gulp` with `npm install -D gulp` so that you can use gulp related goodness on your build script.
- Then create a new file called `gulpfile.js`. This will be your build script. Whatever you want to automate, you will write it here.
- Now put these content on `gulpfile.js`:

```
const gulp = require('gulp');

gulp.task('default', async function(){

});
```

**Explanation:**
The require line is there to tell that we will use gulp functions in our build script, for example, `gulp.task`, `gulp.src` etc. This line will import the necessary files so that we can use them without worries.

The `gulp.task()` function defines a task that we want to do. In this case we are naming this `default`. We can have many tasks in a build script, such as, `css`, `js` etc. A `css` task may minify the css files and `js` may minify the Javascript files. On the other hand, you can include all your tasks inside this `default` function, but this wouldn't be a good idea, since won't be able to run individual tasks. More on that later. But you are adventurous include all of them in one function, go ahead!

We are using `async` functions because it lets Gulp run tasks simultaneously. Our modern computers can handle many tasks at once. So this lets Gulp speed it up.

Now lets put some stuff to print out for us as a demonstration that everything is working:

```
...
gulp.task('default', async function(){
	console.log('This is the default task running!! Hehe!!');
});
```

Now on the terminal just run `gulp`. It should print out many things, but among them you'll find your message is printed as well: `This is the default task running!! Hehe!!`

This way, you can run any Node command to your liking. Basically this is Gulp. All it does is run tasks. That's it. There are other things, but this is basically what we'll be doing.

### Example project

[Click here](https://gitlab.com/adnan360/gulp-examples/-/tree/master/articles/new-project) for a working project example, or check within this directory if you're browsing the repo. Run `npm install` once, then `gulp` to check what it does.
