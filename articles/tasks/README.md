# Tasks

## Overview

We'll get to know how we can have a better grip on our build system with individual tasks.


## Tasks

Let's test out with more tasks. Edit `gulpfile.js` like this:
```
const gulp = require('gulp');

gulp.task('css', async function () {
	console.log('This will be some css tasks running in the future');
});

gulp.task('js', async function () {
	console.log('This will be some js tasks running in the future');
});

gulp.task('default', async function(){
	console.log('This is the default task running!! Hehe!!');
	gulp.series('css','js')();
});
```

Now run `gulp` again. It will print:
```
...
This is the default task running!! Hehe!!
...
This will be some css tasks running in the future
...
This will be some js tasks running in the future
...
```

So, it is running all the tasks listed in `default` as before. And then the `series` call is running `css` and `js`.

Now run `gulp css` and it will print:
```
...
This will be some css tasks running in the future
...
```

This time it is just running just the `css` task.

Similarly, running `gulp js` will run the `js` task and show the `This will be some js tasks running in the future` message.

So what's happening? Under our `default` task we have added a new command `gulp.series('css','js')();`. It tells gulp to run them in series, one by one. Earlier, we have set them as `async`. This lets gulp run them without waiting for another task, all tasks complete relatively faster. When we run `gulp css`, it just runs the `css` task and not any other tasks. When we run `gulp js`, it just runs the `js` task.

Having separate tasks let us run those tasks separately if we ever need it. For example, if you just edited your css files only and want to minify them quickly, you can just run `gulp css` without running other tasks for `js` or `html` or what have you. Neat, isn't it?

