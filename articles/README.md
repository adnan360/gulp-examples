# Gulp by Examples

## [Introduction](./intro)
General introduction on build systems and what Gulp is.

## [Starting a new project](./new-project)
Basic instructions on how to create a new project and implement Gulp.

## [Tasks](./tasks)
Brags on how tasks simplify your life.

## [Inputs and Outputs](./input-and-output)
How we work with inputs and outputs in our everyday life.

## [Modules](./modules)
Hint on how to use almost any module in the world into your build system.

## [Watch files for changes](./watch)
Do things automatically when certain files are edited.

## [Setting up a server](./server)
You'd be amazed how easy it is to setup a server! Plus it reloads automatically when you edit files!

## [Using SASS and LESS for pure CSS comfort](./sass-less)
Describes how this can change your life if you're a CSS junkie.

## [Cache Busting](./cache-busting)
The trick to easily bypass browser cache when you update css/js, without telling all your users to do a hard refresh.

## [JS Transpilation](./js-transpilation)
Shows how you can use tomorrow's JavaScript features today.

## [Image Compression](./image-compression)
Shows an easy way to compress all images automatically for faster websites.

## [Cleanup](./cleanup)
Describes how to cleanup output directory if something messes up.

## [Code Injection](./code-injection)
Demonstrates how to insert code snippets into output files automatically.

## [Templating System](./templating-system)
Shows how to use templates for output.
