# Cleanup

## Overview

We'll have a look at how we can clean our previous output to start fresh with our outputs.


## Basic usage

Sometimes we mess up and our old output folder becomes filled with the stuff from our previous "ill experiments". When that happens we have to start afresh. To do this we need to have a `clean` task that cleans up our output directory. This is common on other build systems as well. If you used `make` before, for compiling C/C++ programs, you'd notice that you're suggested to run `make clean` before doing the build with `make`. We are using the same principle here.

There are many ways to achieve this, but we'll use [del](https://www.npmjs.com/package/del) here.

1. `npm install -D del`
2. `require` it on your `gulpfile.js`: `const del = require('del');`
3. Add task:
```
gulp.task('clean', async function() {
	return del([
		'./dist/*'
	]);
});
```

Now you can run `gulp clean` to run the task. This will clean the files and folders inside `dist` directory so that newer output can be placed there.

**Note:** The glob pattern `**` matches all children and the parent. So it is better to use `somedir/*` as the above example. Details [here](https://www.npmjs.com/package/del#beware).


### Further Reading
- [del package](https://www.npmjs.com/package/del)
- [Delete files and folders](https://github.com/gulpjs/gulp/blob/master/docs/recipes/delete-files-folder.md)
