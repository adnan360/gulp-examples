# Modules

## Overview

We'll actually get "stuff" done on this one.


## Modules

Let's put some module into the mix. We've learned how to get inputs and get outputs. But our example is useless so far. We need to do stuff with our inputs first to get anything done. We have plugins in Gulp for doing "stuff". You will find a module for almost anything you want to do.

Now, let's prepare the `css` task to get an idea of how plugins work. We'll use the [`gulp-clean-css`](https://www.npmjs.com/package/gulp-clean-css) module to minify our CSS files. Go to the module page and the instructions are self explanatory:

1. First install it as a dev dependency (since we are using the module inside our code, so with `-D`): \
`npm install -D gulp-clean-css`
2. Then add this at the top (this code is available on the module page, just copy paste next time): \
`const cleanCSS = require('gulp-clean-css');`
3. Then pipe the inputs into `cleanCSS()` (also available on the module page).

Basically everything you need to know to implement a module is usually on the module page, and basically comprises of these 3 steps.

So our code will be:

```
const cleanCSS = require('gulp-clean-css');
...
gulp.task('css', async function () {
	gulp.src('./src/*.css')
		.pipe(cleanCSS())
		.pipe(gulp.dest('./dist'));
});
...
```

Update the task on your `gulpfile.js`. Now running `gulp css` will not do anything, since we don't have any css files inside the `src` folder. Go ahead and create a test `style.css` inside `src` folder with this content:

```
body {
	background: green;
}
```

Now run `gulp css` and check what outputted inside `dist`. It should be something like:

```
body{background:green;}
```

The CSS you entered just minified before you very own eyes! What kind of sorcery is this?!

Now do the same for JS. For minifying JS, we'll use [`gulp-uglify`](https://www.npmjs.com/package/gulp-uglify). Follow the 3 steps again: (1) install module as dev dependency, (2) include `require()` code, (3) place it into `pipe`.

1. `npm install -D gulp-uglify`
2. `const minifyJS = require('gulp-uglify');`
3. `.pipe(minifyJS())` ...something like this:

```
gulp.task('js', async function () {
	gulp.src('./src/*.js') // path to original files
	.pipe(minifyJS()) // minify the JS files
	.pipe(gulp.dest('./dist')); // output the minified files
});
```

Again, the module page is your friend. Always.


### Further Reading:
- [https://gulpjs.com/docs/en/api/concepts](https://gulpjs.com/docs/en/api/concepts)