const gulp = require('gulp');
// For converting from markdown (.md) files to HTML
const markdown = require('gulp-markdown');
// Add code before and after converted HTML files
var header = require('gulp-header');
var footer = require('gulp-footer');
// For renaming HTML files to index.html
const rename = require("gulp-rename");
// For parsing codes, variables, loops etc. inside html
const nunjucks = require('gulp-nunjucks');
// For minifying CSS
const cleanCSS = require('gulp-clean-css');
// For busting cache
const cacheBuster = require('gulp-cache-bust');
// For running a local server
const connect = require('gulp-connect');
const open = require('gulp-open');
// For reading template parts
const fs = require("fs");
// For cleaning previous output
const del = require('del');

// Output directory
const sourcedir='src';
const outputdir='public';

// Generates HTML for public site
gulp.task('html', async function () {
	// Get content parts for later
	var headerContent = fs.readFileSync(sourcedir+"/parts/md-converted-header.html", "utf8");
	var footerContent = fs.readFileSync(sourcedir+"/parts/md-converted-footer.html", "utf8");

	var jsonData = JSON.parse(fs.readFileSync('data.json', 'utf8'));

	// Converts README.md files inside tutorial
	// folders into HTML pages
	var nextLinkData, prevLinkData;
	var mdData = '# Gulp by Examples\n';
	for(var i = 0; i < jsonData['articles'].length; i++) {
		// Next tutorial link
		nextLinkData = (jsonData['articles'][i+1]) ? {"url":'../'+jsonData['articles'][i+1]['dir'], "title":jsonData['articles'][i+1]['title']} : null;
		prevLinkData = (jsonData['articles'][i-1]) ? {"url":'../'+jsonData['articles'][i-1]['dir'], "title":jsonData['articles'][i-1]['title']} : null;

		// For `articles/README.md`
		mdData += '\n## ['+jsonData['articles'][i]['title']+'](./'+jsonData['articles'][i]['dir']+')\n';
		mdData += jsonData['articles'][i]['desc']+'\n';

		gulp.src('./articles/'+jsonData['articles'][i]['dir']+'/README.md')
			.pipe(markdown())
			.pipe(cacheBuster())
			.pipe(header(headerContent))
			.pipe(footer(footerContent))
			.pipe(nunjucks.compile({
					docTitle: jsonData['articles'][i]['title'],
					nextLink: nextLinkData,
					prevLink: prevLinkData
				}))
			.pipe(rename(jsonData['articles'][i]['dir']+'/index.html'))
			.pipe(gulp.dest(outputdir));

		// Make automatic copies of dist folders from articles folders
		if (fs.existsSync('./articles/'+jsonData['articles'][i]['dir']+'/dist')
			&& ! fs.existsSync(outputdir+'/'+jsonData['articles'][i]['dir']+'/dist')) {
			gulp.src('./articles/'+jsonData['articles'][i]['dir']+'/dist/*')
				.pipe(gulp.dest(outputdir+'/'+jsonData['articles'][i]['dir']+'/dist'));
		}
	}

	fs.writeFile('articles/README.md', mdData, function(err) {
			if(err) {
				return console.log(err);
			}
		});

	gulp.src(sourcedir+'/*.html')
		.pipe(cacheBuster())
		.pipe(nunjucks.compile({
				articles: jsonData["articles"]
			}))
		.pipe(gulp.dest(outputdir))
		.pipe(connect.reload());
});

// Prepares CSS for public site
gulp.task('css', async function () {
	gulp.src(sourcedir+'/css/*.css')
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gulp.dest(outputdir+'/css'))
		.pipe(connect.reload());
});

// Watches files for changes
gulp.task('watch', async function () {
	gulp.watch(['./**/*.md', '!./articles/README.md'], gulp.series('html'));
	gulp.watch('./data.json', gulp.series('html'));
	gulp.watch(sourcedir+'/**/*.html', gulp.series('html'));
	gulp.watch(sourcedir+'/css/*.css', gulp.series('css'));
});

// Starts the server and watches for changes in files
gulp.task('server', async function() {
	connect.server({
		root: 'public',
		port: 9900,
		livereload: true
	});
	gulp.series('watch')();
	gulp.src(__filename)
	.pipe(open({uri: 'http://localhost:9900'}));
});

// Cleans the old output for newer output to be placed
gulp.task('clean', async function() {
	return del([
		outputdir+'/*'
	]);
});

// Runs when someone runs "gulp" on this directory
gulp.task('default', async function(){
	gulp.series('css','html')();
});
